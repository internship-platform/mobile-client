import 'package:flutter/material.dart';

class changePageProvider with ChangeNotifier {
   int _pageIndex = 0;
   int _value=1;
   changePageIndex(index) {
    _pageIndex = index;
    notifyListeners();
  }
  get pageIndex => _pageIndex;

   changeDropDownValue(value){
     _value = value;
     notifyListeners();
   }

   get dropDownValue => _value;
}
