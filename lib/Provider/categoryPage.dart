import 'package:flutter/material.dart';

class categoryPage with ChangeNotifier {
  List<Map<String, String>> categories = [
    {'cat':'All'},
    {'cat': 'Software'},
    {'cat': 'SE'},
    {'cat': 'Mechanical'},
    {'cat': 'AR'},
    {'cat': 'CI'},
    {'cat': 'EE'}
  ];
  static List<Map<String, dynamic>> all = [
    {'cat': 'Software', 'title': 'MobileApp Developer'},
    {'cat': 'Software', 'title': 'React Developer'},
    {'cat': 'Mechanical', 'title': 'Web Developer'},
    {'cat': 'AR', 'title': 'App Developer'},
    {'cat': 'AR', 'title': 'Vue Developer'},
    {'cat': 'EE', 'title': 'Machine Weleder'}
  ];
  List listToShow = all;
  int  selectedIndex=0;
  checkCategory(cat,index) {
    selectedIndex = index;
    listToShow = [];
    if (cat['cat'] != "All") {
      all.forEach((e) {
        if (cat['cat'] == e['cat']) {
          listToShow.add(e);
        }
      });
    }else{
      listToShow = all;
    }
    notifyListeners();
  }
}
