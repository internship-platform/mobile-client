import 'package:flutter/material.dart';
import 'package:ethio_intern/Pages/Intern/home_page.dart';
import 'package:ethio_intern/Provider/categoryPage.dart';
import 'package:ethio_intern/Provider/changePageProvider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Pages/Intern/MyProfile.dart';
import 'Pages/Intern/jobDetail.dart';
import 'Pages/Intern/my_request.dart';
import 'package:intro_slider/intro_slider.dart';
import 'Pages/Intern/sign_up.dart';
import 'Variables/Colors.dart';

Future<bool> isFirstTime;

void main() async {
  Future<SharedPreferences> preferences = SharedPreferences.getInstance();
  isFirstTime = preferences.then((value) => value.get('isFirstTime'));
  print(isFirstTime);
  preferences.then((value) => value.setBool('isFirstTime', true));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<changePageProvider>(
            create: (_) => changePageProvider()),
        ChangeNotifierProvider<categoryPage>(
          create: (_) => categoryPage(),
        )
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: isFirstTime == true ? IntroScreen() : HomeController()),
    );
  }
}

class HomeController extends StatelessWidget {
  final int pageno = 0;
  final pageOptions = [
    HomePage(),
    MyRequest(),
    SignUp(),
    JobDetail(),
    MyProfile()
  ];

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<changePageProvider>(context);

    return Scaffold(
        body: pageOptions[provider.pageIndex],
        bottomNavigationBar: BottomNavigationBar(
            unselectedIconTheme: IconThemeData(color: Colors.white60),
            unselectedItemColor: Colors.white60,
            backgroundColor: MyColor.background,
            selectedIconTheme: IconThemeData(color: MyColor.green),
            selectedItemColor: Colors.white,
            currentIndex: provider.pageIndex,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
              BottomNavigationBarItem(
                icon: Icon(Icons.send),
                label: 'Requests',
              ),
              BottomNavigationBarItem(icon: Icon(Icons.more), label: 'More'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.details), label: 'Details'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), label: 'My Profile')
            ],
            onTap: (index) {
              provider.changePageIndex(index);
            }));
  }
}

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = [];

  void initState() {
    // TODO: implement initState
    super.initState();
    slides.add(
      new Slide(
        title: "ERASER",
        description:
            "Allow miles wound place the leave had. To sitting subject no improve studied limited",
        pathImage: "images/photo_eraser.png",
        backgroundColor: Color(0xfff5a623),
      ),
    );
    slides.add(
      new Slide(
        title: "PENCIL",
        description:
            "Ye indulgence unreserved connection alteration appearance",
        pathImage: "images/photo_pencil.png",
        backgroundColor: Color(0xff203152),
      ),
    );
    slides.add(
      new Slide(
        title: "RULER",
        description:
            "Much evil soon high in hope do view. Out may few northward believing attempted. Yet timed being songs marry one defer men our. Although finished blessing do of",
        pathImage: "images/photo_ruler.png",
        backgroundColor: Color(0xff9932CC0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(slides: this.slides);
  }
}
