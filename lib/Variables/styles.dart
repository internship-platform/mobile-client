import 'package:flutter/material.dart';

import 'Colors.dart';

class ButtonStyles {
  static final ButtonStyle elevatedButtonStyle =
      ElevatedButton.styleFrom(primary: MyColor.green, shape: CircleBorder());
}

class TextColor {
  static TextStyle textStyle(fontSize) {
    return TextStyle(fontSize: fontSize, color: Colors.white);
  }
}
