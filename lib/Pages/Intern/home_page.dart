import 'package:ethio_intern/Provider/categoryPage.dart';
import 'package:ethio_intern/Variables/Colors.dart';
import 'package:ethio_intern/Variables/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<categoryPage>(context);

    return SafeArea(
        child: Scaffold(
            backgroundColor: MyColor.background,
            body:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(28.0),
                    child: Text(
                      'Hello Titus ',
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(
                      Icons.notification_important_sharp,
                      color: MyColor.green,
                      size: 30,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 8, bottom: 8),
                child:
                    Text('Categories', style: TextStyle(color: Colors.white)),
              ),
              Container(
                height: 100,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: provider.categories.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          provider.checkCategory(
                              provider.categories[index], index);
                          // print("${provider.categories[index]['cat']}pressed");
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xff424242),
                              borderRadius: BorderRadius.circular(15.0)),
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          width: 100,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.computer,
                                size: 50,
                                color: provider.selectedIndex == index
                                    ? MyColor.green
                                    : Colors.white,
                              ),
                              Text(
                                '${provider.categories[index]['cat']}',
                                style: TextColor.textStyle(16.0),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
              ),
              Expanded(
                  child: provider.listToShow.length != 0
                      ? ListView.builder(
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade800,
                                  borderRadius: BorderRadius.circular(15)),
                              height: 220,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 8.0, horizontal: 8),
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            child: CircleAvatar(
                                              radius: 40,
                                              backgroundImage: AssetImage(
                                                  'images/banners/banner1.jpg'),
                                            )),
                                      ),
                                      Expanded(
                                        child: ListTile(
                                          title: Text(
                                            '${provider.listToShow[index]['title']}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18),
                                          ),
                                          subtitle: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8.0),
                                            child: Row(
                                              children: [
                                                Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 8,
                                                      vertical: 2),
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey.shade700,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                  ),
                                                  child: Text(
                                                    '${provider.listToShow[index]['cat']}',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 16),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    color: Colors.black.withOpacity(0.2),
                                    child: ListTile(
                                      title: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'KurazTech Company',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 8.0),
                                            child: Row(
                                              children: [
                                                Icon(
                                                  Icons.thumb_up_outlined,
                                                  color: MyColor.green,
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Icon(Icons.thumb_down_outlined,
                                                    color: MyColor.green)
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      subtitle: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 8.0),
                                            child: Row(
                                              children: [
                                                Text('220',
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Text('240',
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'Aug 2020 G.C',
                                          style: TextStyle(
                                              color: Colors.white60,
                                              fontSize: 18),
                                        ),
                                      ), // todo
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 8.0),
                                        child: TextButton(
                                            onPressed: () {},
                                            child: Text(
                                              'Detail ..',
                                              style: TextStyle(
                                                  color: MyColor.green,
                                                  fontSize: 16,
                                                  fontStyle: FontStyle.italic),
                                            )),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            );
                          },
                          itemCount: provider.listToShow.length,
                        )
                      : Center(
                          child: Text(
                          'no',
                          style: TextColor.textStyle(20.0),
                        )))
            ])));
  }
}
