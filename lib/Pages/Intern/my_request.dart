import 'sign_in.dart';
import 'package:ethio_intern/Variables/Colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyRequest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: MyColor.background,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyColor.background,
          title: Center(child: Text('My Request')),
          bottom: TabBar(
            unselectedLabelColor: Colors.white,
            labelColor: MyColor.green,
            indicatorColor: MyColor.green,
            tabs: [Text('Pending'), Text('Confirmed')],
          ),
        ),
        body: TabBarView(
          children: [
            _RequesListBuilder(),
            Container(),
          ],
        ),
      ),
    );
  }
}

class _RequesListBuilder extends StatelessWidget {
  const _RequesListBuilder({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: ListView.builder(
        // physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        addAutomaticKeepAlives: false,
        padding: EdgeInsets.all(14),
        itemCount: 2,
        scrollDirection: Axis.vertical,

        itemBuilder: (BuildContext contex, int index) {
          return Container(
            height: 180,
            child: GestureDetector(
              // onTap: () {
              // },
              child: Card(
                  clipBehavior: Clip.antiAlias,
                  elevation: 0,
                  color: Color(0xFF3B3D4E),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.all(Radius.circular(12))),
                  child: GridTile(
                    footer: Container(
                      color: Colors.black54,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10, bottom: 5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Kuraz tech",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "Technology campany",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w200),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 2),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Icon(
                                    Icons.access_time,
                                    color: Color(0xFF00FC6C),
                                    size: 20,
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        " 04 : 35 PM",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    child: Container(
                      color: Color(0xFF3B3D4E),
                      width: double.infinity,
                      height: 150,
                      child: Image.asset(
                        'images/banners/startup-thinkstock.jpg',
                        fit: BoxFit.cover,
                        alignment: Alignment.centerLeft,
                      ),
                    ),
                  )),
            ),
          );
        },
      ),
    );
  }
}
