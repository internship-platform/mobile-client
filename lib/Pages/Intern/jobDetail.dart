import 'package:ethio_intern/Variables/Colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class JobDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColor.background,
      appBar: AppBar(
        title: Text('Detail'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_sharp, color: MyColor.green),
        ),
        backgroundColor: MyColor.background,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ClipRRect(
                  child: Image.asset(
                    'images/banners/banner1.jpg',
                    width: MediaQuery.of(context).size.width-20,
                    height: 180,
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
              Positioned(
                  bottom: 20,
                  right: 130,
                  child: Icon(
                    Icons.thumb_up_outlined,
                    color: MyColor.green,
                  )),
              Positioned(
                  bottom: 20,
                  right: 80,
                  child: Icon(
                    Icons.favorite_border,
                    color: MyColor.green,
                  )),
              Positioned(
                  bottom: 20,
                  right: 30,
                  child: Icon(
                    Icons.thumb_down_outlined,
                    color: MyColor.green,
                  )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child:
                    Text('Kuraz Tech', style: TextStyle(color: Colors.white)),
              ),
              Row(
                children: [
                  Icon(
                    Icons.watch_later_outlined,
                    color: MyColor.green,
                  ),
                  Text(
                    '4:00pm',
                    style: TextStyle(color: Colors.white),
                  )
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text(
              'Description',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 18.0, right: 18),
            child: Text(
              'A* is a graph traversal and path search algorithm, '
              'which is often used in many fields of computer '
              'science due to its completeness, optimality, '
              'and optimal efficiency. One major practical '
              'drawback is its O(b^d) space complexity,'
              ' as it stores all generated nodes in memory. Wikipedia',
              style: TextStyle(color: Colors.white60, wordSpacing: 2),
            ),
          ),
          SizedBox(height: 20),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              padding: EdgeInsets.only(right: 20),
              width: 150,
              child: ElevatedButton(
                onPressed: () {},
                child: Text(
                  'Apply',
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                    primary: MyColor.green,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
              ),
            ),
          )
        ],
      ),
    );
  }
}
