
import 'package:ethio_intern/Provider/changePageProvider.dart';
import 'package:ethio_intern/Variables/Colors.dart';
import 'package:ethio_intern/Variables/styles.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:provider/provider.dart';

class MyProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: MyColor.background,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(backgroundColor: MyColor.background,
              expandedHeight: 200.0,
              floating: false,
              pinned: true,

              title: Text('My profile'
              ),
              flexibleSpace: FlexibleSpaceBar(

                  background: CircleAvatar(
                  maxRadius: 80,
                  backgroundImage: AssetImage('images/banners/banner1.jpg'),
                ),),
            ),
          ];
        },
        body: ListView(
          children: [

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Software Engineering Student at AASTU ,     Traveler',
                style: TextColor.textStyle(18.0),
                textAlign: TextAlign.center,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 28.0),
                  child: Text(
                    'Skills',
                    style: TextColor.textStyle(24.0),
                  ),
                ),
                TextButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(backgroundColor: MyColor.background,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15),),
                              content: _alertDialogChild()
                            );
                          });
                    },
                    child: Icon(
                      Icons.add,
                      color: MyColor.green,
                    ))
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: (){
                    showDialog(context: context, builder: (context){
                      return AlertDialog(
                          backgroundColor: MyColor.background,
                        content:_alertDialogChild(),
                        actions: [
                          TextButton.icon(onPressed: (){}, icon: Icon(Icons.edit),label: Text('Edit'),)
                        ],
                      );
                    });
                  },
                  child: CircularPercentIndicator(
                    center: Text(
                      'Flutter',
                      style: TextColor.textStyle(14.0),
                    ),
                    radius: 70.0,
                    lineWidth: 5.0,
                    percent: 0.7,
                    progressColor: MyColor.green,
                  ),
                ),
                CircularPercentIndicator(
                  center: Text(
                    'Hasura',
                    style: TextColor.textStyle(14.0),
                  ),
                  radius: 70.0,
                  lineWidth: 5.0,
                  percent: 1,
                  progressColor: MyColor.green,
                ),
                CircularPercentIndicator(
                  center: Text(
                    'Firebase',
                    style: TextColor.textStyle(14.0),
                  ),
                  radius: 70.0,
                  lineWidth: 5.0,
                  percent: 0.4,
                  progressColor: MyColor.green,
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Work Experience',
                style: TextColor.textStyle(24.0),
              ),
            ),
            SizedBox(
              height: 200,
              width: 200,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  Card(
                    elevation: 10,
                    margin: EdgeInsets.all(10),
                    color: Colors.grey.shade800,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Mobile App Development',
                          style: TextColor.textStyle(15.0),
                        ),
                        Text('Masta 3D Studio', style: TextColor.textStyle(15.0)),
                        Text('10/2019 - present',
                            style: TextColor.textStyle(15.0))
                      ],
                    ),
                  ),
                  Card(
                      elevation: 10,
                    margin: EdgeInsets.all(10),
                    color: Colors.grey.shade800,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child:Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Mobile App Development',
                            style: TextColor.textStyle(15.0),
                          ),
                          Text('Masta 3D Studio',
                              style: TextColor.textStyle(15.0)),
                          Text('10/2019 - present',
                              style: TextColor.textStyle(15.0))
                        ],
                      ),

                  ),
                  Card(
                      // shadowColor: MyColor.green,
                      elevation: 10,
                      margin: EdgeInsets.all(10),
                      color: Colors.grey.shade800,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Mobile App Development',
                            style: TextColor.textStyle(15.0),
                          ),
                          Text('Masta 3D Studio',
                              style: TextColor.textStyle(15.0)),
                          Text('10/2019 - present',
                              style: TextColor.textStyle(15.0))
                        ],
                      ),
                    ),

                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Educational Backgrounds',
                style: TextColor.textStyle(24.0),
              ),
            )
          ],
        ),
      ),
    );
  }
  Widget _alertDialogChild(){
    return SizedBox(
        height: 100,
        child: Column(
        children: [
        TextFormField(
        decoration: InputDecoration(
        hintText: 'Skill',
        hintStyle: TextColor.textStyle(13.0)
    ),
    ),
    Consumer<changePageProvider>(
    builder: (context,provider,_){
    return DropdownButton(
    value: provider.dropDownValue,
    onChanged: (value){
    provider.changeDropDownValue(value);
    },
    items: [
    DropdownMenuItem(child: Text('Beginner',style: TextColor.textStyle(13.0),),value: 1,),
    DropdownMenuItem(child: Text('Intermediate',style: TextColor.textStyle(13.0)),value: 2,),
    DropdownMenuItem(child: Text('Advanced',style: TextColor.textStyle(13.0)),value: 3,)
    ]);
    },

    )
    ],
    ));
  }
}
