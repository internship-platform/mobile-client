import 'package:ethio_intern/Variables/Colors.dart';
import 'package:ethio_intern/Variables/styles.dart';
import 'package:flutter/material.dart';

class SignIn extends StatelessWidget {
  TextEditingController userName = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColor.background,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 10, top: 50),
                  child: Text('Sign In',
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Poppins')),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                    padding: EdgeInsets.all(20),
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      controller: userName,
                      decoration: InputDecoration(
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: MyColor.green),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          prefixIcon: Icon(Icons.mail, color: MyColor.green),
                          hintStyle: TextStyle(color: Colors.white),
                          hintText: 'Username'),
                    )),
                Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        controller: password,
                        decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: MyColor.green),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            prefixIcon: Icon(Icons.lock, color: MyColor.green),
                            suffixIcon:
                                Icon(Icons.visibility, color: MyColor.green),
                            hintStyle: TextStyle(color: Colors.white),
                            hintText: 'Password'))),
                Align(
                    alignment: Alignment.centerRight,
                    child: TextButton(
                        onPressed: () {},
                        child: Text(
                          "Forgot Password ?",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ))),
                Center(
                  child: Container(
                      width: 250,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                            backgroundColor: MyColor.green,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20))),
                        child: Text(
                          "Sign In",
                          style: TextStyle(color: Colors.black, fontSize: 25),
                        ),
                        onPressed: () {},
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(children: <Widget>[
                  Expanded(
                      child: Divider(
                    color: Colors.white,
                  )),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Or",
                    style: TextStyle(color: Colors.white),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(child: Divider(color: Colors.white)),
                ]),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ButtonStyles.elevatedButtonStyle,
                      onPressed: () {},
                      child: Text(
                        'f',
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                    ),
                    ElevatedButton(
                      style: ButtonStyles.elevatedButtonStyle,
                      onPressed: () {},
                      child: Text(
                        'g+',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    ElevatedButton(
                      style: ButtonStyles.elevatedButtonStyle,
                      onPressed: () {},
                      child: Text(
                        'in',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: RichText(
                      text: TextSpan(
                    children: [
                      TextSpan(text: "Don't have an account?  "),
                      TextSpan(
                          text: " Create Account",
                          style: TextStyle(color: MyColor.green))
                    ],
                  )),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
