import 'package:ethio_intern/Variables/Colors.dart';
import 'package:ethio_intern/Variables/styles.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController userName = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  bool isObscure = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColor.background,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, top: 50),
            child: Text('Sign Up',
                style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins')),
          ),
          Expanded(
            child: ListView(
              children: [
                _buildForm(userName, 'UserName', Icons.person),
                _buildForm(phone, 'Phone', Icons.phone),
                _buildForm(email, 'Email', Icons.email),
                _buildForm(password, 'Password', Icons.lock),
                _buildForm(confirmPassword, 'Confirm Password', Icons.lock),
                Row(crossAxisAlignment:CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.check_circle_outline,color: MyColor.green,),
                    ),
                    SizedBox(width: 5,),
                    Expanded(
                      child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(text: 'By clicking Sign up, I agree that I have read and accepted the Ethio-Intern  and Privacy Policy '),
                              TextSpan(
                                  text: "Terms of Use",
                                  style: TextStyle(color: MyColor.green)),
                              TextSpan(
                                text:' and',
                              ),
                              TextSpan(
                                  text: " Privacy Policy",
                                  style: TextStyle(color: MyColor.green)
                              )
                            ],
                          )),
                    )

                  ],
                ),
                Center(
                  child: Container(
                      width: 250,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                            backgroundColor: MyColor.green,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20))),
                        child: Text(
                          "Sign Up",
                          style: TextStyle(color: Colors.black, fontSize: 25),
                        ),
                        onPressed: () {},
                      )),
                ),
                Center(
                  child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(text: "Already have an account?  ",style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: "Sign In",
                              style: TextStyle(color: MyColor.green))
                        ],
                      )),
                )
              ],
            ),
          ),


        ],
      ),
    );
  }

  Widget _buildForm(controller, hintText, icon) {
    return Padding(
        padding: EdgeInsets.all(8.0),
        child: TextFormField(
            style: TextStyle(color: Colors.white),
            controller: controller,
            obscureText: icon == Icons.lock ? isObscure : false,
            decoration: InputDecoration(
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: MyColor.green),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                prefixIcon: Icon(icon, color: MyColor.green),
                suffixIcon: icon == Icons.lock
                    ? isObscure
                        ? IconButton(
                            icon: Icon(Icons.visibility_off),
                            onPressed: () {
                              setState(() {
                                isObscure = false;
                              });
                            },
                            color: MyColor.green)
                        : IconButton(
                            icon: Icon(Icons.visibility),
                            onPressed: () {
                              setState(() {
                                isObscure = true;
                              });
                            },
                            color: MyColor.green)
                    : null,
                hintStyle: TextStyle(color: Colors.white),
                hintText: hintText)));
  }
}
