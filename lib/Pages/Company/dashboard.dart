import 'dart:ui';

import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:ethio_intern/Variables/Colors.dart';
import 'package:ethio_intern/Variables/styles.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColor.background,
        title: Text('My DashBoard'),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.notifications_active,
              color: MyColor.green,
            ),
          )
        ],
      ),
      backgroundColor: MyColor.background,
      body: ListView(
        children: [
          SizedBox(
            height: 10,
          ),
          Center(
              child: Text(
            'Welcome Titus',
            style: TextStyle(color: Colors.white, fontSize: 25),
          )),
          Container(
              height: 150,
            width: 150,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage('images/banners/kurazlogo.png'))),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Card(
                color: Color(0xff35486b),
                child: SizedBox(
                  height: 100,
                  width: 150,
                  child: ListTile(
                    title: Icon(Icons.post_add_outlined,color: MyColor.green,size: 40,),
                    subtitle:Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Text('5 Posts',style: TextColor.textStyle(20.0),textAlign: TextAlign.center,),
                    ) ,

                  )
                ),
              ),
              Card(
                  color: Color(0xff35486b),
                  child: SizedBox(
                    height: 100,
                    width: 150,
                    child: ListTile(
                      title: Icon(Icons.send,color: MyColor.green,size: 40,),
                      subtitle: Padding(
                        padding: const EdgeInsets.only(top:15.0),
                        child: Text('3 requests',style: TextColor.textStyle(20.0),textAlign: TextAlign.center,),
                      ),
                    )
                  )),

            ],
          ),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Card(
                  color: Color(0xff35486b),
                  child: SizedBox(
                    height: 100,
                    width: 150,
                    child: ListTile(
                      title: Icon(
                        Icons.comment_sharp,
                        color: MyColor.green,
                        size: 40,
                      ),
                      subtitle: Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Text('2 comments',style: TextColor.textStyle(20.0),textAlign: TextAlign.center),
                      ),
                    ),
                  )),
              Card(
                  color: Color(0xff35486b),
                  child: SizedBox(
                    height: 100,
                    width: 150,
                    child: ListTile(

                    )
                  ))
            ],
          ),
          Text('Recent Activities',style: TextColor.textStyle(15.0),),

        ],
      ),
    );
  }
}
