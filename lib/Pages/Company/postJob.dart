import 'package:ethio_intern/Variables/Colors.dart';
import 'package:flutter/material.dart';

class postJob extends StatefulWidget {
  @override
  _postJobState createState() => _postJobState();
}

class _postJobState extends State<postJob> with SingleTickerProviderStateMixin{
  TextEditingController userName = TextEditingController();
  TextEditingController password = TextEditingController();
  double value=0;
  TabController controller;

  @override
  void initState() {
      super.initState();
      controller = new TabController(vsync:this, length: 2);
    }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        setState(() {
          value-=1;
          controller.animateTo((controller.index -1));
        });
        return;
      },
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: MyColor.background,
          appBar: AppBar(
            bottom: PreferredSize(
              preferredSize: Size(double.infinity, 1.0),
              child: LinearProgressIndicator(
                color: Colors.red,
                value: value,
              ),
            ),
          ),
          body: TabBarView(
              controller: controller,
                physics: NeverScrollableScrollPhysics(),
                children: [
                 Column(
                   children: [
                     TextFormField(
                       style: TextStyle(color: Colors.white),
                       controller: userName,
                       decoration: InputDecoration(
                           focusedBorder: UnderlineInputBorder(
                             borderSide: BorderSide(color: MyColor.green),
                           ),
                           enabledBorder: UnderlineInputBorder(
                             borderSide: BorderSide(color: Colors.white),
                           ),
                           prefixIcon: Icon(Icons.mail, color: MyColor.green),
                           hintStyle: TextStyle(color: Colors.white),
                           hintText: 'Username'),
                     ),
                     TextFormField(
                       style: TextStyle(color: Colors.white),
                       controller: password,
                       decoration: InputDecoration(
                           focusedBorder: UnderlineInputBorder(
                             borderSide: BorderSide(color: MyColor.green),
                           ),
                           enabledBorder: UnderlineInputBorder(
                             borderSide: BorderSide(color: Colors.white),
                           ),
                           prefixIcon: Icon(Icons.mail, color: MyColor.green),
                           hintStyle: TextStyle(color: Colors.white),
                           hintText: 'Username'),
                     ),
                     Center(
                       child: Container(
                           width: 250,
                           child: OutlinedButton(
                             style: OutlinedButton.styleFrom(
                                 backgroundColor: MyColor.green,
                                 shape: RoundedRectangleBorder(
                                     borderRadius: BorderRadius.circular(20))),
                             child: Text(
                               "Sign In",
                               style: TextStyle(color: Colors.black, fontSize: 25),
                             ),
                             onPressed: () {
                               setState(() {
                                 value+=1;
                                 controller.animateTo((controller.index + 1));
                               });
                             },
                           )),
                     )
                   ],
                 ),
                  Text('hello')
                ],
              ),
        ),
      ),
    );
  }
}
